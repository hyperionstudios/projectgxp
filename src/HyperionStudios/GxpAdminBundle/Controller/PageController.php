<?php

namespace HyperionStudios\GxpAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use HyperionStudios\GxpAdminBundle\Form\Type\UsersType;

class PageController extends Controller
{
    public function indexAction()
    {
        
        return $this->render('ProjectGxpAdminBundle:Page:index.html.twig');
    }
    
}
