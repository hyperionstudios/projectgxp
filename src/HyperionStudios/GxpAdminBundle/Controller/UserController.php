<?php

namespace HyperionStudios\GxpAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use HyperionStudios\GxpAdminBundle\Form\Type\UsersType;
use HyperionStudios\GxpAdminBundle\Form\Type\UserType;
use HyperionStudios\GxpBundle\Entity\Ban;

class UserController extends Controller
{
    public function usersAction(Request $request, $page) {
        
        $limit = 5;
        $offset = ($page - 1) * $limit;
        
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository("ProjectGxpBundle:User");
        $users = $userRepo->findBy(array(), array(), $limit, $offset);
        
        $userCount = $userRepo->countAll();
        
        $pages = ceil($userCount / $limit);
        
        $form = $this->createForm(new UsersType($users));
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $selectedUsers = $form->get('users')->getData();
            $uCount = count($selectedUsers );
            if ($uCount >= 1) {
                if ($form->get('edit')->isClicked()) {
                    if ($uCount != 1) {
                        $form->addError(new FormError('You must only select one user.'));
                    }
                    else {     
                        return $this->redirect($this->generateUrl('ProjectGxpAdmin_user_profile', 
                                array('id' => $selectedUsers[0]->getId())
                                ));
                    }
                }
                else if ($form->get('ban')->isClicked()) {
                    
                    return $this->redirect($this->generateUrl('ProjectGxpAdmin_user_ban', array(
                        'userIds' => implode(',', $this->getUsersIds($selectedUsers))
                            )));
                }
                else if ($form->get('delete')->isClicked()) {

                    $usersRemoveMsg = '';
                    foreach ($selectedUsers as $sUser) {
                        $em->remove($sUser);

                        $usersRemoveMsg .= $sUser->getUsername().', ';    
                    }
                    $em->flush();

                    $this->get('session')->getFlashBag()->set('success', 'Removed '. $usersRemoveMsg);
                    return $this->redirect($this->generateUrl('ProjectGxpAdmin_users'));
                
                }
            }
            else {
                $form->addError(new FormError('You must select a user.'));
            }
           //$em = $this->getDoctrine()->getManager();
           //$server = $form->getData();
        }
   
        return $this->render('ProjectGxpAdminBundle:User:users.html.twig', 
                array('users' => $users, 'form' => $form->createView(), 'pages' => $pages, 'page' => $page));
    } 
    
    public function profileAction(Request $request, $id) {
        
        $em = $this->getDoctrine()->getManager();
        $user = $em->find("ProjectGxpBundle:User", $id);
        if (!$user) {
            throw $this->createNotFoundException('The user does not exist');
        }
        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $updatedUser = $form->getData();

            //If password is empty or null, then don't bother with changing password.
            if ($updatedUser->getPassword() != "" || $updatedUser->getPassword() != null) {
                $factory = $this->get('security.encoder_factory');

                $encoder = $factory->getEncoder($user);
                $password = $encoder->encodePassword($updatedUser->getPlainPassword(), $user->getSalt());
                //echo $password;
                $user->setPassword($password);
            }

            //$role = $em->getRepository("ProjectGxpBundle:Role")->findOneByName("User");
            //$user->addRole($role);

            $em->persist($user);
            $em->flush();

                    
        }
        
        return $this->render('ProjectGxpAdminBundle:User:profile.html.twig', 
                array('user' => $user, 'form' => $form->createView()));
    }
    
    public function getUsersIds($users) {
        $ids = array();
        foreach ($users as $user) {
            $ids[] = $user->getId();
        }
        
        return $ids;
    }
    
}
