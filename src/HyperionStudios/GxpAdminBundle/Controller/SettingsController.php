<?php
namespace HyperionStudios\GxpAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use HyperionStudios\GxpAdminBundle\Form\Model\SettingsModel;
use HyperionStudios\GxpAdminBundle\Form\Type\SettingsType;
use HyperionStudios\GxpBundle\Entity\ServerType;
class SettingsController extends Controller {
    
    public function settingsAction(Request $request) {
       
        $em = $this->getDoctrine()->getManager();
        $sTypeRepo = $em->getRepository('ProjectGxpBundle:ServerType');
        $serverTypes = $sTypeRepo->findAll();
        
        $settings = new SettingsModel();
        $settings->setServerTypes($serverTypes);
        
        $form = $this->createForm(new SettingsType(), $settings);
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $settings = $form->getData();
            
            if ($form->get('add_serverType')->isClicked()) {
                if ($form->get('serverType')->isValid()) {
                    $serverType =  $sTypeRepo->findByName($settings->getServerType());
                    if ($serverType) {
                        $form->addError(new FormError('The server type already exists!'));
                    }
                    else {
                        $serverType = new ServerType();
                        $serverType->setName($settings->getServerType());
                        $em->persist($serverType);
                        $em->flush();
                        $this->get('session')->getFlashBag()->set('success', $serverType->getName().' has been added to the game type list.');
                    }
                }
                else {
                    $form->addError(new FormError('Server type you entered is not valid.'));
                }
                
            }
            
            if ($form->get('delete_serverType')->isClicked()) {
                $sTypes = $settings->getServerTypes();
                $stCount = count($sTypes);
                if ($stCount < 1) {
                    $form->addError(new FormError('A server type must be selected.'));
                }
                else {
                    $defaultType = $serverTypes[0];
                    $allServerTypeCount = count($serverTypes);
                    if ($stCount >= $allServerTypeCount) {
                        $form->addError(new FormError('You must keep atleast one server type!'));
                    }
                    else {
                        //see if we can find a free "default" type to move our servers too.
                        for ($i=0; $i < $allServerTypeCount; $i++) {
                            foreach ($sTypes as $sType) {
                                if ($defaultType === $sType) {
                                    $defaultType = $serverTypes[$i];
                                }
                            }
                        }
                         
                        $typesRemoveMsg = '';
                        foreach ($sTypes as $sType) {
                            //move servers to a different type
                            $servers = $sType->getServers();
                            foreach ($servers as $server) {
                                $server->setType($defaultType);
                            }
                            $em->remove($sType);
                                    
                            $typesRemoveMsg .= $sType->getName().',';    
                        }
                        $em->flush();
                        
                        $this->get('session')->getFlashBag()->set('success', 'Removed '. $typesRemoveMsg .' from the game type list.');
                    
                        
                        //redirect to update the type list
                        return $this->redirect($this->generateUrl('ProjectGxpAdmin_settings'));
                    }
       
                }
            }
            
            
        }
        
        return $this->render('ProjectGxpAdminBundle:Settings:settings.html.twig',array(
                    'form' => $form->createView()
            ));
    }
}
