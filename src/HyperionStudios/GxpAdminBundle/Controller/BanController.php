<?php

namespace HyperionStudios\GxpAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use HyperionStudios\GxpAdminBundle\Form\Type\BansType;
use HyperionStudios\GxpBundle\Entity\Ban;

class BanController extends Controller
{
    public function indexAction(Request $request, $page)
    {
        $limit = 5;
        $offset = ($page - 1) * $limit;
        
        $em = $this->getDoctrine()->getManager();
        $banRepo = $em->getRepository("ProjectGxpBundle:Ban");
        $bans = $banRepo->findBy(array(), array(), $limit, $offset);
        
        $banCount = $banRepo->countAll();
        
        $pages = ceil($banCount / $limit);
        
        $form = $this->createForm(new BansType($bans));
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $selectedBans = $form->get('bans')->getData();
            $bCount = count($selectedBans);
            if ($bCount >= 1) {
                if ($form->get('unban')->isClicked()) {
                    return $this->redirect($this->generateUrl('ProjectGxpAdmin_unban', array(
                        'ids' => implode(',', $this->getBanIds($selectedBans))
                            )));
                }
            }
        }
        return $this->render('ProjectGxpAdminBundle:Ban:bans.html.twig', array(
            'bans' => $bans, 'form' => $form->createView(), 'pages' => $pages, 'page' => $page
        ));
    }
    
    public function getBanIds($bans) {
        $ids = array();
        foreach ($bans as $ban) {
            $ids[] = $ban->getId();
        }
        
        return $ids;
    }
    
    public function banUserAction($userIds) {
        $ids = explode(',', $userIds);
        if (empty($ids)) {
            throw $this->createNotFoundException('User id list can not be empty!');
        }
        
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository("ProjectGxpBundle:User");
        $banRepo = $em->getRepository("ProjectGxpBundle:Ban");
        
        $users = $userRepo->findById($ids);
        $bans = $banRepo->findByUser($users);
        
        //if our user exists in our ban array, we need to remove the user.
        $uc = count($users);
        for ($i=0; $i < $uc; $i++) {
            foreach ($bans as $ban) {
                if ($users[$i]->getId() == $ban->getUser()->getId()) {
                    unset($users[$i]);
                    break;
                }
            }
        } 
       
        $uCount = count($users);
        
        if ($uCount < 1) { //no users to be banned!
            $this->get('session')->getFlashBag()->set('fail', 'No users to be banned!');
            return $this->redirect($this->generateUrl('ProjectGxpAdmin_users'));
        }
        else if ($uCount == 1) {
            $banMsg = 'You have ban hammered the user: ';
        }
        else {
            $banMsg = 'You have ban hammered the users: '; 
        }
        
        foreach ($users as $user) {
            $ban = new Ban();
            $ban->setUser($user);
            $em->persist($ban);
            
            $banMsg .= $ban->getUser()->getUsername() .', ';
        }
        $em->flush();

        $this->get('session')->getFlashBag()->set('ban_success', $banMsg);
        
        return $this->redirect($this->generateUrl('ProjectGxpAdmin_users'));
    }
    
    public function unbanAction($ids) {
        $ids = explode(',', $ids);
        if (empty($ids)) {
            throw $this->createNotFoundException('Ban id list can not be empty!');
        }
        
        $em = $this->getDoctrine()->getManager();
        $banRepo = $em->getRepository("ProjectGxpBundle:Ban");
        
        $bans = $banRepo->findById($ids);
        
        $bCount = count($bans);
        if ($bCount == 1) {
            $unbanMsg = 'You have unbanned the users: ';
        }
        else {
            $unbanMsg = 'You have unbanned the users: '; 
        }
        
        foreach ($bans as $ban) {
            $em->remove($ban);
            if ($ban->getUser()) {
                $unbanMsg .= $ban->getUser()->getUsername() .', ';
            }
            else {
                $unbanMsg .= $ban->getIp() .', ';
            }
        }
        
        $em->flush();

        $this->get('session')->getFlashBag()->set('success', $unbanMsg);
        return $this->redirect($this->generateUrl('ProjectGxpAdmin_bans'));
    }
    
}
