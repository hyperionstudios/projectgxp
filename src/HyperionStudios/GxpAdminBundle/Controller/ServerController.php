<?php

namespace HyperionStudios\GxpAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use HyperionStudios\GxpAdminBundle\Form\Type\ServersType;
use HyperionStudios\GxpBundle\Form\Type\ServerType;

class ServerController extends Controller
{
    public function serversAction(Request $request, $page) {
        $limit = 5;
        $offset = ($page - 1) * $limit;
        
        $em = $this->getDoctrine()->getManager();
        $serverRepo = $em->getRepository("ProjectGxpBundle:Server");
        $servers = $serverRepo->findBy(array(), array(), $limit, $offset);
        
        $serverCount = $serverRepo->countAll();
        
        $pages = ceil($serverCount / $limit);
   
        $form = $this->createForm(new ServersType($servers));
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $selectedServers = $form->get('servers')->getData();
            $uCount = count($selectedServers);
            if ($uCount >= 1) {
                if ($form->get('edit')->isClicked()) {
                    if ($uCount != 1) {
                        $form->addError(new FormError('You must only select one server.'));
                    }
                    else {     
                        return $this->redirect($this->generateUrl('ProjectGxpAdmin_server_profile', 
                                array('id' => $selectedServers[0]->getId())
                                ));
                    }
                }
                else if ($form->get('delete')->isClicked()) {
                    
                    $serversRemoveMsg = '';
                    foreach ($selectedServers as $sServer) {
                        $em->remove($sServer);

                        $serversRemoveMsg .= $sServer->getName().', ';    
                    }
                    $em->flush();

                    $this->get('session')->getFlashBag()->set('success', 'Removed '. $serversRemoveMsg);
                    return $this->redirect($this->generateUrl('ProjectGxpAdmin_servers'));
                
                    
                }
            }
            else {
                $form->addError(new FormError('You must select a server.'));
            }
        }
        
        return $this->render('ProjectGxpAdminBundle:Server:servers.html.twig', 
                array('servers' => $servers, 'form' => $form->createView(), 'pages' => $pages, 'page' => $page));
        
    } 
    
    public function profileAction(Request $request, $id) {
        
        $em = $this->getDoctrine()->getManager();
        $server = $em->find("ProjectGxpBundle:Server", $id);
        if (!$server) {
            throw $this->createNotFoundException('The server does not exist');
        }
        $form = $this->createForm(new ServerType(), $server);
        $form->handleRequest($request);
        if ($form->get('newApiKey')->isClicked()) {
            $server->generateApiKey();
            $this->get('session')->getFlashBag()->set('success', "The server's api key has been regenerated.");
            $em->flush();

            //redirect to update the form's apiKey to the new one.
            return $this->redirect($this->generateUrl('ProjectGxpAdmin_server_profile', 
                array('id' => $server->getId())
            ));
        }
        else if ($form->isValid()) {
            $server = $form->getData();
            $this->get('session')->getFlashBag()->set('success', "The server have been successfully edited.");
            $em->flush();
        }           
                
        
        return $this->render('ProjectGxpAdminBundle:Server:profile.html.twig', 
                array('server' => $server, 'form' => $form->createView()));
    }
    
}
