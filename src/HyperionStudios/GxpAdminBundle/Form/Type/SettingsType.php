<?php
// src/HyperionStudios/GxpAdminBundle/Form/Type/SettingsType.php
namespace HyperionStudios\GxpAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class SettingsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('serverTypes', 'entity', array(
            'class' => 'ProjectGxpBundle:ServerType',
            'property' => 'name',
            'multiple'  => true,
            'required' => false,
            'data' => null
            ));
        
        $builder->add('serverType','text', array(
            'required' => false,
            'attr' => array(
                'placeholder' => 'Add Server Type'
            ),
        ));

        $builder->add('add_serverType', 'submit');
        $builder->add('delete_serverType', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //'data_class' => 'HyperionStudios\GxpAdminBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'settingsType';
    }
}