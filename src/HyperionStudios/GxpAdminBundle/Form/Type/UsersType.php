<?php
// src/HyperionStudios/GxpAdminBundle/Form/Type/UsersType.php
namespace HyperionStudios\GxpAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsersType extends AbstractType
{
    private $users;
    
    public function __construct($users) {
        $this->users = $users;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('users', 'entity', array(
                'class' => 'ProjectGxpBundle:User',
                'choices' => $this->users,
                'property' => 'username',
                'multiple'  => true,
                'expanded' => true,
            ));
        $builder->add('edit', 'submit');
        $builder->add('ban', 'submit');
        $builder->add('delete', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //'data_class' => 'HyperionStudios\GxpBundle\Entity\User',
             ));
    }

    public function getName()
    {
        return 'userForm';
    }
}