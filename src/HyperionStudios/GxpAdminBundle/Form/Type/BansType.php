<?php
// src/HyperionStudios/GxpAdminBundle/Form/Type/UsersType.php
namespace HyperionStudios\GxpAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BansType extends AbstractType
{
    private $bans;
    
    public function __construct($bans) {
        $this->bans = $bans;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('bans', 'entity', array(
                'class' => 'ProjectGxpBundle:Ban',
                'choices' => $this->bans,
                'property' => 'id',
                'multiple'  => true,
                'expanded' => true,
            ));
        $builder->add('ban', 'submit');
        $builder->add('unban', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //'data_class' => 'HyperionStudios\GxpBundle\Entity\User',
             ));
    }

    public function getName()
    {
        return 'bansType';
    }
}