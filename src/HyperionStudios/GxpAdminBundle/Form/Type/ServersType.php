<?php
// src/HyperionStudios/GxpAdminBundle/Form/Type/ServersType.php
namespace HyperionStudios\GxpAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ServersType extends AbstractType
{
    private $servers;
    
    public function __construct($servers) {
        $this->servers = $servers;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('servers', 'entity', array(
                'class' => 'ProjectGxpBundle:Server',
                'choices' => $this->servers,
                'property' => 'name',
                'multiple'  => true,
                'expanded' => true,
            ));
        $builder->add('edit', 'submit');
        $builder->add('delete', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //'data_class' => 'HyperionStudios\GxpBundle\Entity\User',
             ));
    }

    public function getName()
    {
        return 'userForm';
    }
}