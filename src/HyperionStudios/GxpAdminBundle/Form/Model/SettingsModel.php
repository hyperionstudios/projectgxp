<?php
// src/HyperionStudios/GxpAdminBundle/Form/Model/ContactModel.php
namespace HyperionStudios\GxpAdminBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class SettingsModel
{
   
    /**
     * @Assert\NotNull()
     */
    protected $serverTypes;
    
    /**
     * @Assert\NotBlank()
     */
    protected $serverType;
    
    public function setServerTypes($serverTypes) {
        $this->serverTypes = $serverTypes;
    }
    
    public function getServerTypes() {
        return $this->serverTypes;
    }
    
    public function setServerType($serverType) {
        $this->serverType = $serverType;
    }
    
    public function getServerType() {
        return $this->serverType;
    }
}
?>
