<?php
namespace HyperionStudios\GxpBundle\Twig\Extensions;

use Symfony\Bridge\Doctrine\RegistryInterface;

class ServerUtilExtension extends \Twig_Extension {
    
    protected $doctrine;

    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }
    
    public function getTotalVotes($server) {
        $total = $this->doctrine->getRepository('ProjectGxpBundle:Server')
            ->getTotalVotes($server);
        echo $total;
        return $total;
    }
    
    
    public function getName() {
        return 'server_util';
    }

    public function getFunctions()
    {
        return array(
            'getTotalVotes' => new \Twig_Function_Method($this, 'getTotalVotes'));
    }
}
