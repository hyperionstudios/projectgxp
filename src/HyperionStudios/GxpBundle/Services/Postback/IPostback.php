<?php

namespace HyperionStudios\GxpBundle\Services\Postback;

interface IPostback {
    
    function buildPacket($server, $vote);
    
    function sendResponse($server, $vote);
    
    function getResponse($server, $vote);
    
}
