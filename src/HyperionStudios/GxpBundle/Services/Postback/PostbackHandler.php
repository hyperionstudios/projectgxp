<?php

namespace HyperionStudios\GxpBundle\Services\Postback;

class PostbackHandler implements IPostback {
    protected $postbackHandlers;
    
    public function __construct($siteName, $jsonSender) { 
        $this->siteName = $siteName;
        $this->postbackHandlers = array(
            'vote_url_postback' => new VoteUrlPostback($siteName, $jsonSender),
            'server_url_postback' => new ServerUrlPostback($siteName, $jsonSender),
            'minecraft_votifier' => new MinecraftVotifier($siteName)
        );
    }

    public function getHandler($handler) {
        return $this->postbackHandlers[$handler];
    }
    
    public function buildPacket($server, $vote) { }
    
    /* 
     * @return boolean
     */
    
    public function sendResponse($server, $vote) {
        $success = false;
        $s = 0;
        $postback = $server->getServerPostback();
        
        if (!empty($postback->getVotifierIp()) && !empty($postback->getVotifierPort()) && 
                !empty($postback->getVotifierPublicKey())) {
            $success = $this->getHandler('minecraft_votifier')->sendResponse($server, $vote);
            $s++;
        }
        if (!empty($postback->getPostbackUrl())) {
            $success = $this->getHandler('vote_url_postback')->sendResponse($server, $vote);
            $s++;
        }
        //return true is no condition were met (no postbacks are set)
        if ($s == 0) {
            return true;
        }
        
        return $success; //on fail return false
        
    }
    
    public function getResponse($server, $vote) {
        if ($vote != null) {
            return $this->getHandler('vote_url_postback')->getResponse($server, $vote);
        }
        else {
            return $this->getHandler('server_url_postback')->getResponse($server, $vote);
        }
    }
    
}
