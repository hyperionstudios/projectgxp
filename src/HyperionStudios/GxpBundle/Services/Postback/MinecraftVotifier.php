<?php

namespace HyperionStudios\GxpBundle\Services\Postback;

class MinecraftVotifier implements IPostback {
    protected $siteName;
    
    public function __construct($siteName) { 
        $this->siteName = $siteName;
    }

    /*
     * Builds a Minecraft votifier packet
     * http://topg.org/php_votifier
     * 
     * Builds a packet to be understandable by votifier
     * @return string
     */
    public function buildPacket($server, $vote) {
        $username = preg_replace("/[^A-Za-z0-9_]+/",'',$vote->getUsername()); 
        
        //set voting time
        $timeStamp = time();

        //create basic required string for Votifier
        $ip = $vote->getIp();
        $packet = "VOTE\n$this->siteName\n$username\n$ip\n$timeStamp\n";

        //fill blanks to make packet lenght 256
        $leftover = (256 - strlen($packet)) / 2;
        while ($leftover > 0) {
            $packet.= "\x0";
            $leftover--;
        }
        
        return $packet;
    }
    
    public function sendResponse($server, $vote) {
        
        $postback = $server->getServerPostback();
        
        //parse the public key (if you change anything here it won't work!)
        $publicKey = wordwrap($postback->getVotifierPublicKey(), 65, "\n", true);
        
        //Note, in order for netbeans to understand this, remove any code indentations!
$publicKey = <<<EOF
-----BEGIN PUBLIC KEY-----
$publicKey
-----END PUBLIC KEY-----
EOF;
                
        $packet = $this->buildPacket($server, $vote);
        
        //encrypt string before send
        openssl_public_encrypt($packet, $crypted, $publicKey);

        //try to connect to server
        $socket = fsockopen($postback->getVotifierIp(), $postback->getVotifierPort(), $errno, $errstr, 3);
        if ($socket) {
            fwrite($socket, $crypted); 	//on success send encrypted packet to server
            fclose($socket);
            return true; 
        }
        return false; //on fail return false
        
    }
    
    public function getResponse($server, $vote) {
        return null;
    }
}
