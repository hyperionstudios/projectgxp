<?php

namespace HyperionStudios\GxpBundle\Services\Postback;

class VoteUrlPostback implements IPostback {
    protected $siteName;
    protected $jsonSender;
    
    public function __construct($siteName, $jsonSender) { 
        $this->siteName = $siteName;
        $this->jsonSender = $jsonSender;
    }

    /*
     * @return string
     */
    public function buildPacket($server, $vote) {
        //nothing fancy...
        return $vote;
    }
    
    public function sendResponse($server, $vote) {
        $postback = $server->getServerPostback();
        
        $json = $this->buildPacket($server, $vote);
       
        return $this->jsonSender->sendJson($postback->getPostbackUrl(), $json);
        
    }
    
    public function getResponse($server, $vote) {
        return $this->buildPacket($server, $vote);
    }

}
