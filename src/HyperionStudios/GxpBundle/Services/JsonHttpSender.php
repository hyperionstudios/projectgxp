<?php
namespace HyperionStudios\GxpBundle\Services;

class JsonHttpSender {
    
    public function sendJson($url, $data) {
        $content = "json=". json_encode($data);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
                array("application/x-www-form-urlencoded"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //curl error SSL certificate problem, verify that the CA cert is OK

        $result     = curl_exec($curl);
        //$response   = json_decode($result);
        //var_dump($response);
        curl_close($curl);
        
        return $result;
        
    }
}