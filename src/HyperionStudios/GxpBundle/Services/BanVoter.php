<?php

namespace HyperionStudios\GxpBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Doctrine\ORM\EntityManager;
use HyperionStudios\GxpBundle\Entity\Ban;
use HyperionStudios\GxpBundle\Entity\User;

/**
 * http://symfony.com/doc/2.3/cookbook/security/voters.html
 */
class BanVoter implements VoterInterface {
    
    private $container;

    private $em;
    
    private $ip;
    private $ban;

    public function __construct(ContainerInterface $container, EntityManager $em) {
        $this->container     = $container;
        $this->em = $em;
    }

    public function supportsAttribute($attribute) {
        // you won't check against a user attribute, so return true
        return true;
    }

    public function supportsClass($class) {
        // your voter supports all type of token classes, so return true
        return true;
    }

    public function vote(TokenInterface $token, $object, array $attributes) {
        $request = $this->container->get('request');
        //Ip isn't going to change so no need for multiple queries.
        if (!$this->ip) {
            $this->ip = $request->getClientIp();
            $this->ban = $this->getBanByIp($request->getClientIp());
        }
        
        if ($this->ban) {
            return VoterInterface::ACCESS_DENIED;
        }

        return VoterInterface::ACCESS_ABSTAIN;
    }

    
    
    /**
     * Checks is user is banned
     * 
     * @param \HyperionStudios\GxpBundle\Entity\User $user
     * @return boolean
     * @throws \Exception
     */
    public function isUserBanned($user) {
        
        $ban = $this->getBanByUser($user);
        //if ban is not null, then the user is banned.
        return $ban != null;
        
    }
    
    /**
     * Checks is ip is banned
     * 
     * @param String $ip
     * @return boolean
     * @throws \Exception
     */
    public function isIpBanned($ip) {
        
        $ban = $this->getBanByIp($ip);
        //if ban is not null, then the user is banned.
        return $ban != null;
        
    }
    
    /**
     * Bans a user
     * 
     * @param \HyperionStudios\GxpBundle\Entity\User $user
     * @throws \Exception
     */
    
    public function banUser($user) {
        
        if ($user == null) {
            throw new \Exception('User object can not be null!');
        }
        if (!($user instanceof User)) {
            throw new \Exception('Parameter must be a User object');
        }
        $ban = new Ban();
        $ban->setUser($user);
        
        $this->em->persist($ban);
        $this->em->flush();
        
    }
    
    /**
     * Bans an ip address
     * 
     * @param String $ip
     * @throws \Exception
     */
    public function banIp($ip) {
        
        if ($ip == null) {
            throw new \Exception('User object can not be null!');
        }

        $ban = new Ban();
        $ban->setIp($ip);
        
        $this->em->persist($ban);
        $this->em->flush();
        
    }
    
    /**
     * Unbans a user
     * 
     * @param \HyperionStudios\GxpBundle\Entity\User $user
     * @return boolean
     * @throws \Exception
     */
    public function unbanUser($user) {

        $ban = $this->getBanByUser($user);
        if ($ban != null) {
            $this->em->remove($ban);
            $this->em->flush();
            return true;
        }
        
        return false;
        
    }
    
    /**
     * Unbans an ip address
     * 
     * @param String $ip
     * @return boolean
     * @throws \Exception
     */
    public function unbanIp($ip) {

        $ban = $this->getBanByIp($ip);
        if ($ban != null) {
            $this->em->remove($ban);
            $this->em->flush();
            return true;
        }
        
        return false;
        
    }
    
    /**
     * Gets a ban for a user
     * 
     * @param \HyperionStudios\GxpBundle\Entity\User $user
     * @return \HyperionStudios\GxpBundle\Entity\Ban
     * @throws \Exception
     */
    public function getBanByUser($user) {
        if ($user == null) {
            throw new \Exception('User object can not be null!');
        }
        if (!($user instanceof User)) {
             throw new \Exception('The parameter must be a User Object');
        }
        
        $banRepo = $this->em->getRepository('ProjectGxpBundle:Ban');
        $ban = $banRepo->findByUser($user);
        
        return $ban;
    }
    
    /**
     * Gets a ban for an ip
     * 
     * @param String $ip
     * @return \HyperionStudios\GxpBundle\Entity\Ban
     * @throws \Exception
     */
    public function getBanByIp($ip) {
        if ($ip == null) {
            throw new \Exception('Ip can not be null!');
        }
        
        $banRepo = $this->em->getRepository('ProjectGxpBundle:Ban');
        $ban = $banRepo->findByIp($ip);
        
        return $ban;
    }
    
    
    
}
