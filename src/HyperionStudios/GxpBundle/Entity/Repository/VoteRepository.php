<?php

// src/HyperionStudios/GxpBundle/Entity/Repository/VoteRepository.php
namespace HyperionStudios\GxpBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use HyperionStudios\GxpBundle\Entity\Vote;

class VoteRepository extends EntityRepository {
    
    public function findOneByServerAndUsernameOrIp($server, $username, $ip) {
        $query = $this->createQueryBuilder('v')
                ->where('v.server = :server')
                ->andWhere('v.username = :username OR v.ip = :ip')
                ->addOrderBy('v.voteTime', 'DESC')
                ->setFirstResult(0)
                ->setMaxResults(1)
                ->setParameter('server',$server)
                ->setParameter('username',$username)
                ->setParameter('ip',$ip);
                   
        //echo $query->getQuery()->getSQL();
        
        return $query->getQuery()->getOneOrNullResult();
    }
    
    public function findOneByServerAndIP($server, $ip) {
        $query = $this->createQueryBuilder('v')
                ->where('v.server = :server')
                ->andWhere('v.ip = :ip')
                ->addOrderBy('v.voteTime', 'DESC')
                ->setFirstResult(0)
                ->setMaxResults(1)
                ->setParameter('server',$server)
                ->setParameter('ip',$ip);
        
        return $query->getQuery()->getOneOrNullResult();
    }
    
    public function findOneByServerAndUsername($server, $username) {
        $query = $this->createQueryBuilder('v')
                ->where('v.server = :server')
                ->andWhere('v.username = :username')
                ->addOrderBy('v.voteTime', 'DESC')
                ->setFirstResult(0)
                ->setMaxResults(1)
                ->setParameter('server',$server)
                ->setParameter('username',$username);
        
        return $query->getQuery()->getOneOrNullResult();
    }
    
    public function countAll() {
        $query = $this->createQueryBuilder('v')
                ->select('COUNT(v.id)');     
        return $query->getQuery()->getSingleScalarResult();
    }
    
    public function findTop($top) {
        $qb = $this->createQueryBuilder('v');
        $qb->select('partial v.{id,username}, SUM(v.votes) AS votes')
            ->setFirstResult(0)
            ->setMaxResults($top)
            ->groupBy('v.username')
            ->orderBy('votes', 'DESC');
        //echo $qb->getQuery()->getSQL();
        $q = $qb->getQuery(Query::HYDRATE_ARRAY)->getResult();

        //rebuild array as a usable object
        $votes = array();
        $i = 0;
        foreach($q as $v) {
            foreach($v as $vote) {

                if ($vote instanceof Vote) {
                    $votes[$i] = $vote;    
                    //echo $vote->getUsername();
                }
                else {
                    //echo $vote .'<br>';
                    $votes[$i]->setVotes($vote);
                    $i++;
                }
            }
        }
        return $votes;
    }
    
}
