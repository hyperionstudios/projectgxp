<?php

// src/HyperionStudios/GxpBundle/Entity/Repository/ServerRepository.php
namespace HyperionStudios\GxpBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\Expr\Join;

class ServerRepository extends EntityRepository {
    /*
     * !!!NOTE THIS IS EXPERIMENTAL AND SHOULD NOT BE USED!!!!
     * Calculating rank dynamicly has proven too compilcated and doesn't work properly.
     * So lets let our backend do it for us at intervals.
     * 
     * http://stackoverflow.com/questions/1293817/mysql-get-users-rank
     * http://stackoverflow.com/questions/19083975/symonfy2-doctrine2-nativequery-resultsetmapping-issue-when-hydrating-as-object
     *
     * protected $baseQuery = "SELECT s1.id,
     *  (
     *      SELECT  COUNT(s2.id)
     *      FROM    ProjectGxpBundle:Server s2
     *     WHERE   (s2.type = s1.type) AND 
     *     (s2.votes >= s1.votes) AND (s2.id >= s1.id)
     * ) AS rank
     * FROM    ProjectGxpBundle:Server s1 ";
     */
    
     /**
     * @return QueryBuilder
     */
    public function getQueryForSearchParameters(QueryBuilder $query, $search) {
  
        if (!empty($search['tags']) && $search['tags'][0] !== "none") {
            foreach ($search['tags'] as $key => $tag) {
                $query->andWhere('s.tags LIKE :tags'.$key)
                    ->setParameter('tags'.$key,'%'.$tag.'%');
            }
        }
        if (!empty($search['version']) && $search['version'] !== "all") {
            $query->andWhere('s.version = :version')
                    ->setParameter('version', $search['version']);
        }
        if (!empty($search['name']) && $search['name'] !== "none") {
            $query->andWhere('s.name LIKE :name')
                    ->setParameter('name', '%'.$search['name'].'%');
        }
        if (!empty($search['country']) && $search['country'] !== "all") {
            $query->andWhere('s.country = :country')
                    ->setParameter('country', $search['country']);
        }
  
        if (!empty($search['sortby'])) {
            switch ($search['sortby']) {
                case "player":
                    $query->addOrderBy('s.playersCount', 'DESC');
                    break;
                case 'version':
                    $query->addOrderBy('s.version', 'DESC');
                    break;
                case 'online':
                    $query->addOrderBy('s.isOnline', 'DESC');
                    break;
                case 'country':
                    $query->addOrderBy('s.country', 'DESC');
                    break;
                case 'uptime':
                    //clamp our value between 0 and 100
                    $query->addSelect('CASE
                        WHEN s.downtime / s.uptime < 0 THEN 0
                        WHEN s.downtime / s.uptime > 100 THEN 100
                        ELSE s.downtime / s.uptime 
                        END AS HIDDEN uptimePercent')
                    ->addOrderBy('uptimePercent', 'ASC');
                    break;
            }
        }
        
        return  $query;
        
    }
    
    public function getServersWithLimitAndType($offset, $max, $tags, $type) {
        //gathers all server objects of that type within a limit
        //$dql = 'SELECT SUM(v.votes) as totalVotes FROM  Vote v ';
        //$subquery = $this->getManager()->createQuery($dql);
        
        $query = $this->createQueryBuilder('s')
            ->select('s, SUM(v.votes) AS HIDDEN totalV')
            ->innerJoin('s.type', 't', Join::WITH, 't.name = :type')
            ->leftJoin('s.votes', 'v')
            ->setFirstResult($offset)
            ->setMaxResults($max)
            ->orderBy('totalV', 'DESC')
            ->groupBy('s')
            ->setParameter('type', $type);
        if ($tags !== "none") {
            $format = '%%%s%%';

            $query->andWhere('s.tags LIKE :tags')
            ->setParameter('tags',sprintf($format, $tags));
        }
        return $query->getQuery()->getResult();
    }
    
    
    
    public function findByTypeWithLimit($offset, $max, $type) {
         $query = $this->createQueryBuilder('s')
            ->select('s, SUM(v.votes) AS HIDDEN totalV')
            ->innerJoin('s.type', 't', Join::WITH, 't.name = :type')
            ->leftJoin('s.votes', 'v')
            ->setFirstResult($offset)
            ->setMaxResults($max)
            ->orderBy('totalV', 'DESC')
            ->groupBy('s')
            ->setParameter('type', $type);
         
        return $query->getQuery()->getResult();
    }
    
    public function findByTypeAndSearchWithLimit($offset, $max, $type, array $search) {
         $query = $this->createQueryBuilder('s')
            ->select('s, SUM(v.votes) AS HIDDEN totalV')
            ->innerJoin('s.type', 't', Join::WITH, 't.name = :type')
            ->leftJoin('s.votes', 'v')
            ->setFirstResult($offset)
            ->setMaxResults($max)
            ->groupBy('s')
            ->setParameter('type', $type);
        if (!empty($search)) { 
            $query = $this->getQueryForSearchParameters($query, $search);
        }
        //orderby votes AFTER user sortby option.
        $query->addOrderBy('totalV', 'DESC');
        
        //echo $query->getQuery()->getSQL();
        return $query->getQuery()->getResult();
    }
    
    
    public function findTop($top) {
        $query = $this->createQueryBuilder('s')
                ->select('s, SUM(v.votes) AS HIDDEN totalV')
                ->leftJoin('s.votes', 'v')
                ->setFirstResult(0)
                ->setMaxResults($top)  
                ->orderBy('totalV', 'DESC')
                ->groupBy('s');
        return $query->getQuery()->getResult();
    }
    
    public function countServers($type, $tags, $version, $name) {
        $query = $this->createQueryBuilder('s')
                ->select('COUNT(s.id)')
                ->innerJoin('s.type', 't', Join::WITH, 't.name = :type')
                ->setParameter('type',$type);
        
        $query = $this->getQueryForSearchParameters($query, $tags, $version, $name);    
        
        return $query->getQuery()->getSingleScalarResult();
    }
    
    public function countAll() {
        $query = $this->createQueryBuilder('s')->select('COUNT(s.id)');

        return $query->getQuery()->getSingleScalarResult();
    }
    
    public function getVersions($type) {
        $s_versions = $this->createQueryBuilder('s')
                ->select('DISTINCT s.version')
                ->innerJoin('s.type', 't', Join::WITH, 't.name = :type')
                ->orderBy('s.version', 'DESC')
                ->setParameter('type', $type)
                ->getQuery()
                ->getResult();
        
        $versions = array();
        foreach ($s_versions as $v) {
            if (!empty($v['version'])) {
                $versions[$v['version']] = $v['version'];
            }
        }

        return $versions;
    }
    
    public function getTags($type) {
        $serverTags = $this->createQueryBuilder('s')
                         ->select('s.tags')
                         ->innerJoin('s.type', 't', Join::WITH, 't.name = :type')
                         ->setParameter('type', $type)
                         ->getQuery()
                         ->getResult();
        // print_r($serverTags);
        $tags = array();
        foreach ($serverTags as $serverTag)
        {
            $tags = array_merge($serverTag['tags'], $tags);
        }

        foreach ($tags as &$tag)
        {
            $tag = trim($tag);
        }

        return $tags;
    }

    public function getTagWeights($tags) {
        $tagWeights = array();
        if (empty($tags)) {
            return $tagWeights;
        }
        
        foreach ($tags as $tag)
        {
            $tagWeights[$tag] = (isset($tagWeights[$tag])) ? $tagWeights[$tag] + 1 : 1;
        }
        // Shuffle the tags
        uksort($tagWeights, function() {
            return rand() > rand();
        });

        $max = max($tagWeights);

        // Max of 5 weights
        $multiplier = ($max > 5) ? 5 / $max : 1;
        foreach ($tagWeights as &$tag)
        {
            $tag = ceil($tag * $multiplier);
        }

        return $tagWeights;
    }
   
    
}
