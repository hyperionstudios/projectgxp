<?php

// src/HyperionStudios/GxpBundle/Entity/Repository/UserRepository.php
namespace HyperionStudios\GxpBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class BanRepository extends EntityRepository {
    
    public function countAll() {
        $query = $this->createQueryBuilder('b')->select('COUNT(b.id)');

        return $query->getQuery()->getSingleScalarResult();
    }

}
