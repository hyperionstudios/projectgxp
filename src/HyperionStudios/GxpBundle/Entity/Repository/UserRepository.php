<?php

// src/HyperionStudios/GxpBundle/Entity/Repository/UserRepository.php
namespace HyperionStudios\GxpBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository {
    
    public function countAll() {
        $query = $this->createQueryBuilder('u')->select('COUNT(u.id)');

        return $query->getQuery()->getSingleScalarResult();
    }

}
