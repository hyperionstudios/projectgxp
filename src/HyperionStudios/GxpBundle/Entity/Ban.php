<?php
// src/HyperionStudios/GxpBundle/Entity/Ban.php
namespace HyperionStudios\GxpBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use HyperionStudios\GxpBundle\Entity\User;

/**
 *
 * @ORM\Table(name="pgxp_bans")
 * @ORM\Entity(repositoryClass="HyperionStudios\GxpBundle\Entity\Repository\BanRepository")
 * @UniqueEntity(fields="user", message="User already been banned.")
 * @UniqueEntity(fields="ip", message="Ip already been banned.")
 */
class Ban
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
     /**
     * @ORM\Column(type="string", length=40, unique=true, nullable=true)
     * @Assert\Length(
     *  min=3,
     *  max=40,
     *  minMessage = "Ip Address must be at least {{ limit }} characters long.",
     *  maxMessage = "Ip Address cannot be longer than {{ limit }} characters."
     * )   
     */
    protected $ip;
    
    /**
     * @ORM\OneToOne(targetEntity="User", cascade={"persist"})
     * @Assert\Valid()
     */
    protected $user;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set ip
     *
     * @param string $ip
     * @return Ban
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }
    
    /**
     * Set user
     *
     * @param string $user
     * @return User
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    
}