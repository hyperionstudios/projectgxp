<?php

namespace HyperionStudios\GxpBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * VoteUser
  * @ORM\Entity(repositoryClass="HyperionStudios\GxpBundle\Entity\Repository\VoteRepository")
 * @ORM\Table(name="pgxp_votes")
 */
class Vote implements \JsonSerializable
{
     /**
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 3,
     *      max = 60,
     *      minMessage = "Username must be atleast 3 characters long.",
     *      maxMessage = "Username must be no longer than 60 characters long.")
     */
    protected $username;
    
     /**
     * @ORM\Column(type="string", length=45)
     */
    protected $ip;
    
    /**
    * @ORM\Column(type="integer")
    */
    protected $votes;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $voteTime;
    
    /**
     * @ORM\ManyToOne(targetEntity="Server", inversedBy="votes")
     */
    protected $server;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Vote
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }
    
        /**
     * Set ip
     *
     * @param string $ip
     * @return Vote
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

     /**
     * Set votes
     *
     * @param integer $votes
     * @return Vote
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;
    
        return $this;
    }
    
    /**
     * Add vote
     *
     * @param integer $vote
     * @return Vote
     */
    public function addVote($vote)
    {
        $this->votes += $vote;
    
        return $this;
    }
    
    /**
     * Remove vote
     *
     * @param integer $vote
     * @return Vote
     */
    public function removeVote($vote)
    {
        $this->votes -= $vote;
    
        return $this;
    }

    /**
     * Get votes
     *
     * @return integer 
     */
    public function getVotes()
    {
        return $this->votes;
    }
    
    /**
     * Set voteTime
     *
     * @param \DateTime $voteTime
     * @return Vote
     */
    public function setVoteTime($voteTime)
    {
        $this->voteTime = $voteTime;
    
        return $this;
    }

    /**
     * Get voteTime
     *
     * @return \DateTime 
     */
    public function getVoteTime()
    {
        return $this->voteTime;
    }

    /**
     * Set server
     *
     * @param \HyperionStudios\GxpBundle\Entity\Server $server
     * @return Vote
     */
    public function setServer($server = null)
    {
        $this->server = $server;
    
        return $this;
    }

    /**
     * Get server
     *
     * @return \HyperionStudios\GxpBundle\Entity\Server 
     */
    public function getServer()
    {
        return $this->server;
    }

    public function jsonSerialize() {
        return array(
            'user' => array(
                'username' => $this->username,
                'ip' => $this->ip,
            ),
            'server' => array(
                'id' => $this->server->getId(),
                'api_key' => $this->server->getAPIKey()
            ),
            'votes' => $this->votes,
            'unix_time' => $this->voteTime->getTimestamp(),
            'datetime' => $this->voteTime
        );
    }

}