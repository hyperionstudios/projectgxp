<?php

// src/HyperionStudios/GxpBundle/Entity/Server.php
namespace HyperionStudios\GxpBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * ProjectGxp/GxpBundle\Entity\ServerType
 * 
 * @ORM\Entity()
 * @ORM\Table(name="pgxp_servertypes")
 *
 */
class ServerType
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;
    
    /**
     * @ORM\OneToMany(targetEntity="Server", mappedBy="type")
     */
    protected $servers;

    public function __construct()
    {

    }

    public function setName($name) {
        $this->name = $name;
    }
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->name;
    }


    public function setServer($server) {
        $this->server = $server;
    }

    /**
     * @inheritDoc
     */
    public function getServers()
    {
        return $this->servers;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

}