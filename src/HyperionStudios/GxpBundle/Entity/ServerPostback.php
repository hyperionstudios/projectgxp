<?php
// src/HyperionStudios/GxpBundle/Entity/ServerPostback.php
namespace HyperionStudios\GxpBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectGxp/GxpBundle\Entity\ServerPostback
 * @ORM\Entity
 * @ORM\Table(name="pgxp_serverpostbacks")
 *
 * This is a class to hold basic info on servers.
 */
class ServerPostback
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
     /**
     * @ORM\Column(type="string", length=40)
     * @Assert\Length(
     *  min=3,
     *  max=40,
     *  minMessage = "IP address must be at least {{ limit }} characters",
     *  maxMessage = "IP address name cannot be longer than {{ limit }} characters"
     * ) 
     */
    protected $votifierIp;
    
     /**
     * @ORM\Column(type="smallint", nullable=true, options={"unsigned"=true})
     * @Assert\Range(
     *  min = 1,
     *  max = 65535,
     *  minMessage = "Port must be higher than {{ limit }}",
     *  maxMessage = "Port must be lower than {{ limit }}"
     * )
     */
    protected $votifierPort;
    
    /**
    * @ORM\Column(type="text", length=1024, nullable=true)
    * @Assert\Length(
    *  max=1024,
    *  maxMessage = "Public key cannot be longer than {{ limit }} characters"
    * )
    */
    protected $votifierPublicKey;
    
    /**
    * @ORM\Column(type="string", length=100, nullable=true)
    * @Assert\Length(
    *  max=100,
    *  maxMessage = "Postback URL cannot be longer than {{ limit }} characters"
    * )
     * @Assert\Url()
     */
    protected $postbackUrl;

    
    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set votifierIp
     *
     * @param string $votifierIp
     * @return ServerPostback
     */
    public function setVotifierIp($votifierIp)
    {
        $this->votifierIp = $votifierIp;
    
        return $this;
    }

    /**
     * Get votifierIp
     *
     * @return string 
     */
    public function getVotifierIp()
    {
        return $this->votifierIp;
    }
    
        /**
     * Set votifierport
     *
     * @param integer $votifierPort
     * @return ServerPostback
     */
    public function setVotifierPort($votifierPort)
    {
        $this->votifierPort = $votifierPort;
    
        return $this;
    }

    /**
     * Get votifierPort
     *
     * @return integer
     */
    public function getVotifierPort()
    {
        return $this->votifierPort;
    }

     /**
     * Set votifierPublicKey
     *
     * @param string $votifierPublicKey
     * @return ServerPostback
     */
    public function setVotifierPublicKey($votifierPublicKey)
    {
        $this->votifierPublicKey = $votifierPublicKey;
    
        return $this;
    }

    /**
     * Get votifierPublicKey
     *
     * @return string 
     */
    public function getVotifierPublicKey()
    {
        return $this->votifierPublicKey;
    }
    
    /**
     * Set postbackUrl
     *
     * @param string $postbackUrl
     * @return ServerPostback
     */
    public function setPostbackUrl($postbackUrl)
    {
        $this->postbackUrl = $postbackUrl;
    
        return $this;
    }

    /**
     * Get postbackUrl
     *
     * @return string 
     */
    public function getPostbackUrl()
    {
        return $this->postbackUrl;
    }
}