<?php

// src/HyperionStudios/GxpBundle/Entity/Server.php
namespace HyperionStudios\GxpBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * ProjectGxp/GxpBundle\Entity\Server
 * @ORM\Entity(repositoryClass="HyperionStudios\GxpBundle\Entity\Repository\ServerRepository")
 * @ORM\Table(name="pgxp_servers")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *  fields={"name"},
 *  message="This name has already been listed."
 * )
 * 
 * @Assert\Callback(methods={"canConnect"})
 */
class Server implements \JsonSerializable
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

     /**
     * @ORM\ManyToOne(targetEntity="ServerType", inversedBy="servers")
     */
    protected $type;
    
    /**
     * @ORM\OneToOne(targetEntity="ServerPostback", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    protected $postback;
    
    /**
     * @ORM\Column(type="string", length=40)
     * @Assert\Regex(
     *   pattern="/^[A-Za-z]+[\w\-\s]*[A-Za-z0-9]+$/",
     *   message="Server names must start with a letter and can only 
             contain alphanumeric, underscore, and dash characters and can only end with an alphanumeric character."
     *  )
     * })
     * @Assert\Length(
     *  min=3,
     *  max=40,
     *  minMessage = "Server name must be at least {{ limit }} characters long.",
     *  maxMessage = "Server name cannot be longer than {{ limit }} characters."
     * )
     * @Assert\NotBlank(message="Server name should not be blank.")
     */
    protected $name;

     /**
     * @ORM\Column(type="string", length=40)
     * @Assert\Length(
     *  min=5,
     *  max=40,
     *  minMessage = "Ip Address must be at least {{ limit }} characters long.",
     *  maxMessage = "Ip Address cannot be longer than {{ limit }} characters."
     * )
     * @Assert\NotBlank(message="Ip Address should not be blank.")     
     */
    protected $ip;
    
    /**
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     * @Assert\Range(
     *  min = 1,
     *  max = 65535,
     *  minMessage = "Port must be higher than {{ limit }}",
     *  maxMessage = "Port must be lower than {{ limit }}"
     * )
     * @Assert\NotBlank(message="Port number should not be blank.")
     */
    protected $port;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Url()
     */
    protected $url;
    
    /**
     * @ORM\OneToOne(targetEntity="BannerImage", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    protected $bannerImage;
    
    /**
     * @ORM\Column(type="string", length=5)
     * @Assert\Country
     * @Assert\NotBlank(message="You must select a country!")
     */
    protected $country;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *  min=35,
     *  minMessage = "Description must be at least {{ limit }} characters long."
     * )
     * @Assert\NotBlank(message="Description should not be blank.")
     */
    protected $description;
    
    /**
    * @ORM\Column(type="string", length=15, nullable=true)
    */
    protected $version;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $creationDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastPing;
    
     /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isOnline;
    
    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    protected $uptime = 0;
    
     /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    protected $downtime = 0;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $playersCount;
    
     /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $maxPlayersCount;
    
     /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    protected $onlinePlayers;

    /**
     * @ORM\OneToMany(targetEntity="Vote", mappedBy="server", cascade={"persist", "remove"})
     */
    protected $votes;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $accumulatedVotes = 0;
    
     /**
      * @ORM\Column(type="simple_array", nullable=true)
      * @Assert\All({
      *  @Assert\NotBlank(message="Tags should not be blank."),
      *  @Assert\Length(
      *   min=3,
      *   max=20,
      *   minMessage = "Tags must be at least {{ limit }} characters long.",
      *   maxMessage = "Tags must be no longer than {{ limit }} characters."
      *  ),
      * @Assert\Regex(
      *   pattern="/^[A-Za-z][\w\-\s][A-Za-z0-9]+$/",
      *   message="Tags must start with a letter and can only 
             contain alphanumeric, underscore, and dash characters and can not end with an underscore."
      *  )
      * })
      * @Assert\NotNull(message="You need at least one tag!")
      */
    protected $tags;

     /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="servers")
     */
    protected $user;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $apiKey;
    
    public function __construct()
    {
        $this->bannerImage = new BannerImage();
    }

    public function setName($name) {
        $this->name = $name;
    }
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->name;
    }


    public function setUser($user) {
        $this->user = $user;
    }

    /**
     * @inheritDoc
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Server
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Server
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set version
     *
     * @param string $version
     * @return Server
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Server
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set lastPing
     *
     * @param \DateTime $lastPing
     * @return Server
     */
    public function setLastPing($lastPing)
    {
        $this->lastPing = $lastPing;

        return $this;
    }

    /**
     * Get lastPing
     *
     * @return \DateTime
     */
    public function getLastPing()
    {
        return $this->lastPing;
    }

    /**
     * Set uptime
     *
     * @param integer $uptime
     * @return Server
     */
    public function setUptime($uptime)
    {
        $this->uptime = $uptime;

        return $this;
    }

    /**
     * Get uptime
     *
     * @return integer
     */
    public function getUptime()
    {
        return $this->uptime;
    }
    
        /**
     * Set downtime
     *
     * @param integer $downtime
     * @return Server
     */
    public function setDowntime($downtime)
    {
        $this->downtime = $downtime;

        return $this;
    }

    /**
     * Get downtime
     *
     * @return integer
     */
    public function getDowntime()
    {
        return $this->downtime;
    }
    
    /**
     * Get uptimePercentage
     * 
     * @return float
     */
    
    public function getUptimePercentage() {
        /**
         Downtime = 10 tests
         Uptime = 15 tests

         Downtime (in %)
         = [10/30] x 100
         = 33.33%

         Uptime (in %)
         = 1 - 0.3333
         = 66.67%
        */
        $up = 1;
        $down = 0;
        if ($this->downtime) {
            $down = $this->downtime / ($this->uptime +  $this->downtime);
            $up = round(1 - $down,2);
            //echo $down ." ". $up;
            if ($up < 0) { $up = 0; }
            else if ($up > 1) { $up = 1; }
        }
        
      
        return $up;
    }

    /**
     * Set type
     *
     * @param ServerType $type
     * @return Server
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return ServerType
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
   * Set ServerPostBack
   *
   * @param ServerPostback $postback
   * @return Server
   */
    public function setServerPostback($postback)
    {
        $this->postback = $postback;
        return $this;
    }

    /**
     * Get ServerPostBack
     *
     * @return ServerPostback
     */
    public function getServerPostback()
    {
        return $this->postback;
    }

    /**
     * Set playersCount
     *
     * @param string $playersCount
     * @return Server
     */
    public function setPlayersCount($playersCount)
    {
        $this->playersCount = $playersCount;

        return $this;
    }

    /**
     * Get playersCount
     *
     * @return string
     */
    public function getPlayersCount()
    {
        return $this->playersCount;
    }

    /**
     * Set onlinePlayers
     *
     * @param array $onlinePlayers
     * @return Server
     */
    public function setOnlinePlayers($onlinePlayers)
    {
        $this->onlinePlayers = $onlinePlayers;

        return $this;
    }

    /**
     * Get onlinePlayers
     *
     * @return array
     */
    public function getOnlinePlayers()
    {
        return $this->onlinePlayers;
    }



    /**
     * Set maxPlayersCount
     *
     * @param integer $maxPlayersCount
     * @return Server
     */
    public function setMaxPlayersCount($maxPlayersCount)
    {
        $this->maxPlayersCount = $maxPlayersCount;
    
        return $this;
    }

    /**
     * Get maxPlayersCount
     *
     * @return integer 
     */
    public function getMaxPlayersCount()
    {
        return $this->maxPlayersCount;
    }

    /**
     * Add vote
     *
     * @param Vote $vote
     * @return Server
     */
    public function addVote($vote)
    {
        if ($this->votes->contains($vote)) {
            $this->votes->add($vote);
        }
        return $this;
    }
    
    /**
     * Add vote
     *
     * @param Vote $vote
     * @return Server
     */
    public function removeVote($vote)
    {
        if ($this->votes->contains($vote)) {
            $this->votes->remove($vote);
        }
    
        return $this;
    }
    
    /**
     * Get votes
     *
     * @return Vote Array
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Get total Votes count
     * 
     * @return integer
     */
    public function getTotalVotes() {
        $total = 0;
        foreach ($this->votes as $vote) {
            $total += $vote->getVotes();
        }
        return $total;
    } 
    
    /**
     * Increment accumulated Votes
     * 
     * @return Server
     */
    public function incAccumulatedVotes($vote) {
        $this->accumulatedVotes += $vote;
        return $this;
    } 
    
    /**
     * Set total accumulated Votes
     * 
      * @return Server
     */
    public function setAccumulatedVotes($vote) {
        $this->accumulatedVotes = $vote;
        return $this;
    } 
    
    /**
     * Get total accumulated Votes
     * 
     * @return integer
     */
    public function getAccumulatedVotes() {
        return $this->accumulatedVotes;
    } 
    
    /**
     * Set tags
     *
     * @param array $tags
     * @return Server
     */
    public function setTags($tags)
    {
        //force tags to be lowercased.
        foreach ($tags as $key => $value) {
            $tags[$key] = strtolower($value);
        }
        //Make sure no dupilcate tags are in the array.
        $this->tags = array_unique($tags);
    
        return $this;
    }

    /**
     * Get tags
     *
     * @return array 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Server
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set port
     *
     * @param integer $port
     * @return Server
     */
    public function setPort($port)
    {
        $this->port = $port;
    
        return $this;
    }

    /**
     * Get port
     *
     * @return integer 
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set bannerImage
     *
     * @param string $bannerImage
     * @return Server
     */
    public function setBannerImage($bannerImage)
    {
        $this->bannerImage = $bannerImage;
    
        return $this;
    }

    /**
     * Get bannerImage
     *
     * @return string 
     */
    public function getBannerImage()
    {
        return $this->bannerImage;
    }

    /**
     * Set isOnline
     *
     * @param boolean $isOnline
     * @return Server
     */
    public function setIsOnline($isOnline)
    {
        $this->isOnline = $isOnline;
    
        return $this;
    }

    /**
     * Get isOnline
     *
     * @return boolean 
     */
    public function isOnline()
    {
        return $this->isOnline;
    }
   
    /**
     * Get isOnline
     *
     * @return boolean 
     */
    public function getIsOnline()
    {
        return $this->isOnline;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Server
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Set apiKey
     *
     * @param string $apiKey
     * @return Server
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    
        return $this;
    }
    
    /**
     * @ORM\PrePersist
     * Generate apiKey
     *
     * @return Server
     */
    public function generateApiKey() {
        $rand = mt_rand();
        //server id doesn't exist here yet, but the ip is pretty unique.
        $key = $this->ip . $rand . $this->user->getId();
        $this->apiKey = base64_encode($key);
        
        return $this;
    }
    
    /**
     * Get apiKey
     *
     * @return string 
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }
    
    public function canConnect(ExecutionContextInterface $context) {
        $fp = @fsockopen($this->ip, $this->port, $errno, $errstr);
        if (!$fp) {
             $context->addViolationAt('ip', 'Ip address is unreachable. (Make sure your server is actually online!)', array(), null);
             $context->addViolationAt('port', 'Port is unreachable.', array(), null);
        }
    }    
    
    public function jsonSerialize() {
        return array(
            'server' => array(
                'id' => $this->getId(),
                'api_key' => $this->getAPIKey(),
                'online' => $this->isOnline(),
                'monthlyVotes' => $this->getTotalVotes(),
                'totalVotes' => $this->getAccumulatedVotes(),
                'uptime' => $this->getUptimePercentage(),
                'lastPing' => $this->getLastPing(),
                'version' => $this->getVersion(),
                'playersCount' => $this->getPlayersCount(),
                'maxPlayersCount' => $this->getName(),
                'players' => $this->getOnlinePlayers(),              
            ),
        );
    }
}