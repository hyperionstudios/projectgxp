<?php
// src/HyperionStudios/GxpBundle/Form/Type/ServerPostbackType.php
namespace HyperionStudios\GxpBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ServerPostbackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('votifierIp', 'text', array(
            'required' => false,
            'attr' => array(
                'placeholder' => 'Votifier ip',
            )
            ));
        $builder->add('votifierPort', 'text', array(
            'required' => false,
            'attr' => array(
                'placeholder' => 'Votifier port',
            )
            ));
        $builder->add('votifierPublicKey', 'textarea', array(
            'required' => false,
            'attr' => array(
                'placeholder' => "Votifier public key",
                'rows' => "5"
                )
            ));
        $builder->add('postbackUrl', 'url', array(
            'required' => false,
            'label' => 'Vote notification url',
            'attr' => array(
                'placeholder' => 'Vote notification url',
            )
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HyperionStudios\GxpBundle\Entity\ServerPostback',
            'error_bubbling' => false
        ));
    }

    public function getName()
    {
        return 'serverPostbackType';
    }
}

