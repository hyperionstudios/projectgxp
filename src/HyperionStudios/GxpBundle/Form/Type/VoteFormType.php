<?php
// src/HyperionStudios/GxpBundle/Form/Type/VoteFormType.php
namespace HyperionStudios\GxpBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class VoteFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', 'text', array(
            'label' => false,
            'attr' => array(
                'placeholder' => '(IGN) In-Game Name'
            )
        ));
        $builder->add('submit', 'submit', array(
            'label' => 'Vote',
            'attr' => array('class' => 'btn btn-primary'),
        ));
    }

    public function getName() {
        return 'voteForm';
    }

}