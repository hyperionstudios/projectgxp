<?php
// src/HyperionStudios/GxpBundle/Form/Type/ServerPostbackType.php
namespace HyperionStudios\GxpBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BannerImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'file', array(
            'required' => false,
            'error_bubbling' => false,
            'attr' => array(
                'accept' => 'image/*',
                'novalidate' => 'novalidate'
            ),
            'label' => 'List Banner Image'
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HyperionStudios\GxpBundle\Entity\BannerImage',
        ));
    }

    public function getName()
    {
        return 'bannerImageType';
    }
}

