<?php
// src/HyperionStudios/GxpBundle/Form/Type/TagsType.php
namespace HyperionStudios\GxpBundle\Form\Type;

 
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use HyperionStudios\GxpBundle\Form\DataTransformer\TagsTransformer;
 
class TagsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->appendClientTransformer(new TagsTransformer());
        
    }
 
    public function getParent()
    {
        return 'text';
    }
 
    public function getName()
    {
        return 'tags';
    }
}