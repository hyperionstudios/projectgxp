<?php

// src/HyperionStudios/GxpBundle/Form/Type/RegistrationFormType.php
namespace HyperionStudios\GxpBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use HyperionStudios\GxpBundle\Form\DataTransformer\TagsTransformer;
use HyperionStudios\GxpBundle\Form\Type\ServerPostbackType;
use HyperionStudios\GxpBundle\Form\Type\BannerImageType;

class ServerType extends AbstractType
{
    private $validationGroup;
    
    public function __construct($validationGroup = 'default') {
        $this->validationGroup = $validationGroup;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add('type', 'entity', array(
            'label' => 'Choose game:', 
            'class' => 'ProjectGxpBundle:ServerType',
            'property' => 'name',
            'attr' => array(
                'class' => 'form-control'
            ),)
                );
        $builder->add('name', 'text', array(
            'attr' => array(
                'placeholder' => 'Server Name',
            ),
            'required' => true)
                );
        $builder->add('country', 'country', 
                array('attr' => array(
                    'class' => 'form-control'
                    ),
                    'preferred_choices' => array('US','AU','CA'))
                );
        
        $builder->add('ip', 'text', array(
            'attr' => array(
                'placeholder' => 'IP Address',
            ),

            'required' => true)
                );
        $builder->add('port', 'text', array(
            'attr' => array(
                'placeholder' => 'Port Number eg: 25565',
            ),
            'required' => true)
                );
        $builder->add('url', 'url', array(
            'required' => false,
            'attr' => array(
                'placeholder' => 'Website URL',
            ),)
                );
        $builder->add('description', 'textarea', array(
            'attr' => array(
                'placeholder' => 'Description',
            ),)
                );
        
        $builder->add('bannerImage', new BannerImageType()); 
        $builder->add('serverPostback', new ServerPostbackType());
        
        $builder->add(
                $builder->create('tags', 'textarea', array(
                    'attr' => array('placeholder' => 'Use commas to seperate tags.')
                    )
                )->addModelTransformer(new TagsTransformer())
            );
        $builder->add('apiKey', 'text', array(
            'read_only' => true,
            'disabled' => true
        ));
        $builder->add('newApiKey', 'submit',array(
            'label' => 'Generate Key'
        ));
        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HyperionStudios\GxpBundle\Entity\Server',
            'validation_groups' => function(FormInterface $form){
                if(!empty($this->validationGroup)) {
                    return array('Default', $this->validationGroup);
                }
                else {
                    return array('Default');
                }
            }
        ));
    }
    
    public function getName()
    {
        return 'serverForm';
    }

}

