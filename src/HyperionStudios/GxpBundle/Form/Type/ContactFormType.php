<?php
// src/HyperionStudios/GxpBundle/Form/Type/ContactFormType.php
namespace HyperionStudios\GxpBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
            'attr' => array(
                'placeholder' => 'Name',
            )));
        $builder->add('email', 'email', array(
            'attr' => array(
                'placeholder' => 'Email',
            )));
        $builder->add('description', 'textarea', array(
            'attr' => array(
                'placeholder' => 'Description',
                'rows' => '5'
            )));
        $builder->add('recaptcha', 'ewz_recaptcha',array(
                'attr' => array(
                    'options' => array(
                        'theme' => 'white'
                    ))
            ));
        
        $builder->add('submit', 'submit', array(
            'attr' => array(
                'class' => 'btn btn-primary'
            )));
    }

    public function getName()
    {
        return 'contactForm';
    }
}