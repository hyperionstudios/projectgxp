<?php

// src/HyperionStudios/GxpBundle/Form/Type/RegistrationFormType.php
namespace HyperionStudios\GxpBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use HyperionStudios\GxpBundle\Form\DataTransformer\TagsTransformer;

class ServerSearchType extends AbstractType
{
    private $versions;
    private $sortByOptions;
    
    public function __construct($versions = null,
            $sortByOptions = null) {
        $this->versions = $versions;
        if (empty($sortByOptions)) {
            $sortByOptions = array(
                'players' => 'players',
                'version' => 'version',
                'online'  => 'online',
                'country' => 'country',
                'uptime'  => 'uptime');
        }
        $this->sortByOptions = $sortByOptions;
        

    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name', 'text', array(
            'required' => false,
            'attr' => array(
                'placeholder' => 'Search by name',
            ),
            'empty_data' => "none"
        ));  
        
        $builder->add('version', 'choice', array(
            'required'    => false,
            'empty_value' => 'Search by Version',
            'empty_data' => "all",
            'choices' => $this->versions,
        ));
        
        $builder->add('country', 'country', array(
            'required'    => false,
            'attr' => array(
                 'class' => 'form-control'
            ),
            'empty_value' => 'Search by Country',
            'empty_data' => "all",
            'preferred_choices' => array('US','AU','CA'))
        );
        
        $builder->add('sortBy', 'choice', array(
            'required' => false,
            'attr' => array(
                 'class' => 'form-control'
            ),
            'empty_value' => 'Sort By',
            'empty_data' => "default",
            'choices' => $this->sortByOptions
        ));
        
        $builder->add(
                $builder->create('tags', 'text', array(
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'Search by Tags; Use commas to seperate tags.'
                        ),
                    'empty_data' => "none"
                    ))->addModelTransformer(new TagsTransformer())
            );
        
        
        $builder->add('submit', 'submit');
        
    }

    public function getName()
    {
        return 'serverSearchForm';
    }
    
}

?>
