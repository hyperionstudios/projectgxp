<?php
// src/HyperionStudios/GxpBundle/Form/Type/LoginType.php
namespace HyperionStudios\GxpBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username');
        $builder->add('password', 'password');
    }

    public function getName()
    {
        return 'loginForm';
    }
}