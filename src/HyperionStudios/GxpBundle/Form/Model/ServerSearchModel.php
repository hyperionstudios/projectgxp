<?php
// src/HyperionStudios/GxpBundle/Form/Model/ServerSearchModel.php
namespace HyperionStudios\GxpBundle\Form\Model;

class ServerSearchModel
{
    protected $name;
    
    //set a default value as for some reason the form's choice empty data doesn't work?
    protected $version = 'all';
    
    protected $tags;
    
    protected $country = 'all';
    
    protected $sortBy = 'default';

    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }
    
    public function setVersion($version) {
        $this->version = $version;
    }
    public function getVersion() {
        return $this->version;
    }
    
    public function setTags($tags) {
        $this->tags = $tags;
    }
    public function getTags() {
        return $this->tags;
    }
    
    public function setCountry($country) {
        $this->country = $country;
    }
    public function getCountry() {
        return $this->country;
    }
    
    public function setSortBy($sortBy) {
        $this->sortBy = $sortBy;
    }
    
    public function getSortBy() {
        return $this->sortBy;
    }
    
}

