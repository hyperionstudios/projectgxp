<?php
// src/HyperionStudios/GxpBundle/Controller/AuthController.php

namespace HyperionStudios\GxpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;
use Symfony\Component\Form\FormError;
use HyperionStudios\GxpBundle\Form\Type\VoteFormType;
use HyperionStudios\GxpBundle\Form\Type\ServerType;
use HyperionStudios\GxpBundle\Entity\Vote;
use HyperionStudios\GxpBundle\Entity\Server;

class ServerController extends Controller
{
    public function serverAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $server = $em->find("ProjectGxpBundle:Server", $id);
        
        if (!$server) {
            throw $this->createNotFoundException('The server does not exist');
        }
        
        $vote = null;
        
        $form = $this->createForm(new VoteFormType(), new Vote());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $formVote = $form->getData();
            //Set this this a old date to fix the nullpointer bug when we compare timestamps.
            $formVote->setVoteTime(new \DateTime('2000-01-01'));
            $formVote->setServer($server);
           
            $ip = $this->container->get('request')->getClientIp();
            
            $vote = $em->getRepository("ProjectGxpBundle:Vote")->findOneByServerAndUsernameOrIp($server, 
                    $formVote->getUsername(), $ip);
            
            //echo var_dump($vote);
            if ($vote == null ) {
                //no ip or username match make a new entry
                $vote = $formVote;          
                $em->persist($vote);
            }
            else if ($vote->getUsername() !== $formVote->getUsername()) {
                //we want to perseve the IGN that was inputed.
                $nVote = $em->getRepository("ProjectGxpBundle:Vote")->findOneByServerAndUsername($server, 
                        $formVote->getUsername());
                //We need to preserve the votetime from the query returned by the ip.
                if ($nVote != null) {
                    $nVote->setVoteTime($vote->getVoteTime());
                    $vote = $nVote;
                }
                else {
                    //ip matched but no username, so let's add a new entry but reuse the matched query's vote time.
                    $formVote->setVoteTime($vote->getVoteTime());
                    $vote = $formVote;
                    $em->persist($vote);
                }
            } 

            $now = new \DateTime("now");
            $diff = $now->getTimestamp() - $vote->getVoteTime()->getTimestamp();
            $minVoteTime = $this->container->getParameter('min_vote_time');

            if ($diff >= $minVoteTime) {
                //our vote is succesful
                $this->get('session')->getFlashBag()->set('vote_success', 'Thank you for voting!');
                
                $server->incAccumulatedVotes(1);
                
                $vote->setVoteTime($now);
                $vote->addVote(1);
                $vote->setIp($ip);
                
                $em->flush();
                
                if (!$this->get('postback_handler')->sendResponse($server, $vote)) {
                    $form->addError(new FormError("Your vote has been counted, but an error has occured sending it to the server."));
                }
            }
            else {
                //calculate the time difference to vote again.
                $d = $vote->getVoteTime()->add(new \DateInterval("PT".$minVoteTime."S"))->diff($now);
                $form->addError(new FormError("Sorry, but you have already voted.<br>"
                        . "You must wait ". $d->format("<b>%h</b> hours, <b>%i</b> minutes, and <b>%s</b> seconds") . " to vote again."));
            }

        }

        return $this->render('ProjectGxpBundle:Server:server.html.twig', 
                array('server' => $server,
                    'form' => $form->createView(),
                    'vote' => $vote
                ));
    }
   
    public function createServerAction(Request $request)
    {
        $securityContext = $this->container->get('security.context');
        if($securityContext->isGranted('IS_AUTHENTICATED_FULLY') ){
            $server = new Server();
            $form = $this->createForm(new ServerType('registration'), $server);
            $form->handleRequest($request);
            if ($form->isValid()) {
               $em = $this->getDoctrine()->getManager();
               $server = $form->getData();

               $server->setUser($this->getUser());
               $server->setCreationDate(new \DateTime("now"));

               $em->persist($server);
               
               $em->flush();
               
               $this->get('session')->getFlashBag()->set('create_success', 'This is your new server listing!');
               
               return $this->redirect($this->generateUrl('ProjectGxp_server', array('id' => $server->getId())));
            }
            return $this->render('ProjectGxpBundle:Server:createServer.html.twig', array('form' => $form->createView()));
        }
        else {
            return $this->redirect($this->generateUrl('ProjectGxp_login'));
        }
        
    }
    
    public function editServerAction(Request $request, $id)
    {
        $securityContext = $this->container->get('security.context');
        if($securityContext->isGranted('IS_AUTHENTICATED_FULLY') ){
            $em = $this->getDoctrine()->getManager();
            
            $server = $em->find("ProjectGxpBundle:Server", $id);
            if (!$server) {
                throw $this->createNotFoundException('The server does not exist');
            }
            elseif ($server->getUser() == $this->getUser()) {
                $form = $this->createForm(new ServerType(), $server);
                $form->handleRequest($request);
                                
                if ($form->get('newApiKey')->isClicked()) {
                    $server->generateApiKey();
                    $this->get('session')->getFlashBag()->set('edit_success', "Your server's api key has been regenerated.");
                    $em->flush();
                    
                    //redirect to update the form's apiKey to the new one.
                    return $this->redirect($this->generateUrl('ProjectGxp_editServer', array('id' => $server->getId())));
                }
                else if ($form->isValid()) {
                    $server = $form->getData();
                    $this->get('session')->getFlashBag()->set('edit_success', "Your server have been successfully edited.");
                    $em->flush();
                }           
                
                return $this->render('ProjectGxpBundle:Server:editServer.html.twig', 
                        array('form' => $form->createView(),
                            'server' => $server,
                            )
                        );
            }
        }
        else {
            return $this->redirect($this->generateUrl('ProjectGxp_login'));
        }
        
    }
    
     public function removeServerAction(Request $request, $id, $confirmed)
    {
        $session = $request->getSession();
        $confirmedDoubleCheck = $session->get('confirmed');
        
        //var_dump($confirmed);
        
        $securityContext = $this->container->get('security.context');
        if($securityContext->isGranted('IS_AUTHENTICATED_FULLY') ){
            $em = $this->getDoctrine()->getManager();
            $server = $em->find("ProjectGxpBundle:Server", $id);
            if (!$server) {
                throw $this->createNotFoundException('The server does not exist');
            }
            
            /*
             * If our session "confirmed" is set and is the id of this server,
             * then we can go ahead and remove the server.
             * Also, we need a confirm check to make sure the user clicked the button, 
             * so refreshing doesn't accidently delete it.
             */
            if ($confirmedDoubleCheck == $id && $confirmed === "1") {
                $session->remove('confirmed');
                
                $em->remove($server);
                $em->flush();
                
                $this->get('session')->getFlashBag()->set('success', 'Your server have been successfully removed.');
                return $this->redirect($this->generateUrl('ProjectGxp_servers'));
             }
             if ($confirmed == false) {
                $session->set('confirmed', $id);
             }
             
             return $this->render('ProjectGxpBundle:Server:removeServer.html.twig', 
                     array('server' => $server)
                     );
            }
        
        return $this->redirect($this->generateUrl('ProjectGxp_login'));
        
    }
    
    
    public function searchServerAction(Request $request)
    {
        $response = new Response();
        
        $serverSearch = new ServerSearchModel();
        $form = $this->createForm(new SearchServerFormType(), $serverSearch);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $serverSearch = $form->getData();
            $session = $this->getRequest()->getSession();
            $session->set('searchForm', $serverSearch);
        }
        return $this->render('ProjectGxpBundle:Server:removeServer.html.twig', 
                     array('searchForm' => $form)
                     );
    }

}