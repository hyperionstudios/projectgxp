<?php
// src/HyperionStudios/GxpBundle/Controller/VoteApiController.php

namespace HyperionStudios\GxpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use HyperionStudios\GxpBundle\Entity\Vote;
use HyperionStudios\GxpBundle\Entity\Server;

class ApiController extends Controller {
    public function voteAction(Request $request, $id, $apikey, $username) {
        $em = $this->getDoctrine()->getManager();
        
        $server = $em->find("ProjectGxpBundle:Server", $id);
        
        if (!$server) {
            throw $this->createNotFoundException('The server does not exist');
        }
        
        if ($apikey === $server->getApiKey()) {
            $vote = $em->getRepository("ProjectGxpBundle:Vote")->findOneByServerAndUsername($server, $username);

            if (!$vote) {
                throw $this->createNotFoundException('The username does not exist');
            }
        }
        else {
            throw $this->createNotFoundException('Apikey does not match!');
        }
        
        return new JsonResponse($this->get('postback_handler')->getResponse($server, $vote));
    }
    
    public function serverAction(Request $request, $id, $apikey) {
        $em = $this->getDoctrine()->getManager();
        
        $server = $em->find("ProjectGxpBundle:Server", $id);
        
        if (!$server) {
            throw $this->createNotFoundException('The server does not exist');
        }
        
        if ($apikey !== $server->getApiKey()) {
            throw $this->createNotFoundException('Apikey does not match!');
        }
        
        return new JsonResponse($this->get('postback_handler')->getResponse($server, null));
    }
  
}