<?php
// src/HyperionStudios/GxpBundle/Controller/UserController.php

namespace HyperionStudios\GxpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use HyperionStudios\GxpBundle\Entity\User;
use HyperionStudios\GxpBundle\Form\Type\UserFormType;

class UserController extends Controller
{
    public function userProfileAction(Request $request, $id)
    {
        
        $em = $this->getDoctrine()->getManager();
        $user = $em->find("ProjectGxpBundle:User", $id);
        if (!$user) {
            throw $this->createNotFoundException('The user does not exist');
        }
        
        $securityContext = $this->container->get('security.context');
        if($securityContext->isGranted('IS_AUTHENTICATED_FULLY') ){
            if ($this->getUser()->getId() == $user->getId()) {
                $form = $this->createForm(new UserFormType(), $user);
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $updatedUser = $form->getData();


                    //If password is empty or null, then don't bother with changing password.
                    if ($updatedUser->getPassword() != "" || $updatedUser->getPassword() != null) {
                        $factory = $this->get('security.encoder_factory');

                        $encoder = $factory->getEncoder($user);
                        $password = $encoder->encodePassword($updatedUser->getPlainPassword(), $user->getSalt());
                        //echo $password;
                        $user->setPassword($password);
                    }

                    //$role = $em->getRepository("ProjectGxpBundle:Role")->findOneByName("User");
                    //$user->addRole($role);

                    $em->persist($user);
                    $em->flush();

                    $successMsg = "Updated Successfully";

                    return $this->render('ProjectGxpBundle:User:userProfile.html.twig', 
                        array(
                            'form' => $form->createView(),
                            'user' => $user,
                            'successMsg' => $successMsg
                        ));
                }
                return $this->render('ProjectGxpBundle:User:userProfile.html.twig', 
                    array(
                        'form' => $form->createView(),
                        'user' => $user,
                    ));
            }
        }
        return $this->redirect($this->generateUrl('ProjectGxp_login'));
    }

}