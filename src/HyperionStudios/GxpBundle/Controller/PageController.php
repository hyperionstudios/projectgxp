<?php
// src/HyperionStudios/GxpBundle/Controller/PageController.php

namespace HyperionStudios\GxpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

use HyperionStudios\GxpBundle\Form\Model\ContactModel;
use HyperionStudios\GxpBundle\Form\Model\ServerSearchModel;
use HyperionStudios\GxpBundle\Form\Type\ContactFormType;
use HyperionStudios\GxpBundle\Form\Type\ServerSearchType;

use HyperionStudios\GxpBundle\Entity\Server;

class PageController extends Controller
{
    public function indexAction(Request $request, $type, $page, $tags, $version, $name, $country, $sortby)
    {
        //convert tags into an array
        $tags = explode(',', $tags);
        
        $maxServersOnPage = $this->container->getParameter('maxServersPerPage');
        
        $offset = ($page - 1) * $maxServersOnPage;
        
        $em = $this->getDoctrine()->getManager();
        
        $serverRepo = $em->getRepository('ProjectGxpBundle:Server'); 
        
        $search = array(
            'tags' => $tags,
            'version' => $version,
            'name' => $name,
            'country' => $country,
            'sortby' => $sortby
            );
        
        $servers = $serverRepo->findByTypeAndSearchWithLimit($offset, $maxServersOnPage, $type, $search);

        $serverCount = $serverRepo->countServers($type, $tags, $version, $name);   

        $pages = ceil($serverCount / $maxServersOnPage);
        
        $allTags = $serverRepo->getTags($type);
        $tagWeights = $serverRepo->getTagWeights($allTags);
        
        //Get all server types.
        $serverTypes = $em->getRepository('ProjectGxpBundle:ServerType')->findAll();
         
        return $this->render('ProjectGxpBundle:Page:index.html.twig', 
                array('servers' => $servers,
                    'serverTypes' => $serverTypes,
                    'type'  => $type,
                    'page'  => $page,
                    'pages' => $pages,
                    'tags'  => implode(',',$tags),
                    'tagWeights' => $tagWeights,
                    'countOfServersOfType' => $serverCount,
                    'maxServersOnPage' => $maxServersOnPage,
                    'query' => array(
                        'version' => $version, 
                        'name'    => $name, 
                        'country' => $country, 
                        'sortby'  => $sortby)
                ));
    }
    
    public function searchBarAction(Request $request, $type) {
        $em = $this->getDoctrine()->getManager();
        $serverRepo = $em->getRepository('ProjectGxpBundle:Server');    
        
        $versions = $serverRepo->getVersions($type);
        
        $form = $this->createForm(new ServerSearchType($versions), new ServerSearchModel()) ;
        $form->handleRequest($request);
      
        if ($form->isSubmitted()) {
            $search = $form->getData();
            //var_dump($search);
            return $this->redirect($this->generateUrl('ProjectGxp_servers', array(
                'type' => $type,
                'version' => $search->getVersion(),
                'tags' => implode(',', $search->getTags()),
                'name' => $search->getName(),
                'country' => $search->getCountry(),
                'sortby' => $search->getSortBy()
            )));
        }

        return $this->render('ProjectGxpBundle:Form:searchServer.html.twig', 
                array(
                    'type' => $type,
                    'form' => $form->createView())
                );
    }
    
    public function contactAction(Request $request)
    {
        $form = $this->createForm(new ContactFormType(), new ContactModel());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $contact = $form->getData();
            
            $recevier =  $this->container->getParameter('mailer_recevier');
            
            try {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact by '. $contact->getName())
                    ->setFrom('no-reply@projectgxp.com')
                    ->setTo($recevier)
                    ->setContentType("text/html")    
                    ->setBody($this->renderView(
                        'ProjectGxpBundle:Email:contact.html.twig',
                        array('contact' => $contact)
                    ));
                $this->get('session')->getFlashBag()->set('email_success', 'Thank you for sending us an email! :D.<br> We shall respond shortly.');
                $this->get('mailer')->send($message);
                
                return $this->redirect($this->generateUrl('ProjectGxp_contact'));
            }
            catch(\Swift_TransportException $e) {
                $form->addError(new FormError("Failed to send email!"));
            }
            
        }
        return $this->render('ProjectGxpBundle:Page:contact.html.twig', array('form' => $form->createView()));
    }
    
    public function statsAction() {
        $stats = array(
            'servers_count' => array(),
            'top_servers' => array(),
            'votes' => array()
            );
        
        $em = $this->getDoctrine()->getManager();
        
        $serverTypes = $em->getRepository('ProjectGxpBundle:ServerType')->findAll();
        
        $serverRepo = $em->getRepository('ProjectGxpBundle:Server');
        $voteRepo = $em->getRepository('ProjectGxpBundle:Vote');
        
        $stats['servers_count']['all'] = $serverRepo->countAll();
        
        foreach ($serverTypes as $type) {
            $serCount = $serverRepo->countServers($type->getName(), null, null, null);
            $stats['servers_count'][$type->getName()] = $serCount;
        }
        
        $stats['top_servers'] = $serverRepo->findTop(5);
        
        $stats['votes'] = $voteRepo->findTop(5);   
        

        
        return $this->render('ProjectGxpBundle:Page:stats.html.twig', array('stats' => $stats));
    }

    
}