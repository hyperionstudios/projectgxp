<?php
// src/HyperionStudios/GxpBundle/Controller/AuthController.php

namespace HyperionStudios\GxpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use HyperionStudios\GxpBundle\Form\Model\RegistrationModel;
use HyperionStudios\GxpBundle\Form\Type\RegistrationFormType;
use Symfony\Component\Security\Core\SecurityContext;

class AuthController extends Controller
{
    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
            
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render('ProjectGxpBundle:Auth:login.html.twig', array(
            'error'         => $error,
        ));
    }
    public function registerAction(Request $request)
    {
        $registration = new RegistrationModel();
        $form = $this->createForm(new RegistrationFormType(), $registration);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $registration = $form->getData();
            $factory = $this->get('security.encoder_factory');
            $user = $registration->getUser();

            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
            //echo $password;
            $user->setPassword($password);
            
            $role = $em->getRepository("ProjectGxpBundle:Role")->findOneByName("User");
            $user->addRole($role);
            
            $em->persist($user);
            $em->flush();
            
            $this->get('session')->getFlashBag()->set('success', 'You have successfully registered.');

            return $this->redirect($this->generateUrl('ProjectGxp_login'));
        }
        return $this->render('ProjectGxpBundle:Auth:register.html.twig', array('form' => $form->createView()));
    }

}