//note: we have to conform to this favor of bbcode to make everything work https://github.com/milesj/decoda/
var wbbOpt = {
    buttons: "bold,italic,underline,strike,sup,sub,|,img,video,link,|,bullist,numlist,|,fontcolor,fontsize,fontfamily,|,justifyright,|,quote,table,removeFormat",
    allButtons: {
        video: {
            transform: {
              '<iframe src="http://www.youtube.com/embed/{SRC}" width="640" height="480" frameborder="0"></iframe>':'[video="youtube"]{SRC}[/video]'
            }
        },
        numlist: {
            transform : {
                '<ol>{SELTEXT}</ol>':'[olist]{SELTEXT}[/olist]',
                '<li>{SELTEXT}</li>':'[li]{SELTEXT}[/li]'
            }
        },
        bullist: {
            transform : {
                '<ul>{SELTEXT}</ul>':"[list]{SELTEXT}[/list]",
                '<li>{SELTEXT}</li>':'[li]{SELTEXT}[/li]'
            }
        },
        fontfamily: {
            transform: {
                '<font face="{FONT}">{SELTEXT}</font>':'[font="{FONT}"]{SELTEXT}[/font]'
            }
        },
        fs_verysmall: {
            transform: {
                '<font size="1">{SELTEXT}</font>':'[size="10"]{SELTEXT}[/size]'
            }
        },
        fs_small: {
            transform: {
                '<font size="2">{SELTEXT}</font>':'[size="15"]{SELTEXT}[/size]'
            }
        },
        fs_normal: {
            transform: {
                '<font size="3">{SELTEXT}</font>':'[size="20"]{SELTEXT}[/size]'
            }
        },
        fs_big: {
            transform: {
                '<font size="4">{SELTEXT}</font>':'[size="25"]{SELTEXT}[/size]'
            }
        },
        fs_verybig: {
            transform: {
                '<font size="6">{SELTEXT}</font>':'[size="29"]{SELTEXT}[/size]'
            }
        },

   }
};
