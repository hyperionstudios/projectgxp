#!/bin/bash

export COMPOSER_HOME="$DATA_DIR/.composer"
 
if [ ! -f "$DATA_DIR/composer.phar" ]; then
    curl -s https://getcomposer.org/installer | /usr/bin/php -- --install-dir=$DATA_DIR
else
    /usr/bin/php $DATA_DIR/composer.phar self-update
fi
 
cd $REPO_DIR 
/usr/bin/php $DATA_DIR/composer.phar install

chmod -R 0777 $REPO_DIR/app/cache
chmod -R 0777 $REPO_DIR/app/logs

/usr/bin/php $REPO_DIR/app/console cache:clear --env=dev
/usr/bin/php $REPO_DIR/app/console cache:clear --env=prod
/usr/bin/php $REPO_DIR/app/console doctrine:schema:update --force
